package whatchacooking;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException, ProcessingException {
        JsonSchemaFactory factory = JsonSchemaFactory.byDefault();

        JsonNode schemaJson = JsonLoader.fromResource("/step.json");
        JsonSchema schema = factory.getJsonSchema(schemaJson);

        ProcessingReport report;


        JsonNode data = JsonLoader.fromPath(args[0]);
        report = schema.validate(data);
        System.out.println(report);
    }
}
